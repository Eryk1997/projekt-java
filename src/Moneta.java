import java.util.Arrays;

public class Moneta {
    private double wartosc;

    public Moneta(double wartosc) {
        if (Arrays.asList(0.01, 0.02, 0.05, 0.1, 0.2, 0.5, 1.0, 2.0, 5.0, 10.0, 20.0, 50.0).contains(wartosc))
            this.wartosc = wartosc;
    }

    //metoda zwraca wartosc danej monety
    public double getWartosc(){
        return this.wartosc;
    }

}
