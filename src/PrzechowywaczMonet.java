import java.util.ArrayList;


public class PrzechowywaczMonet {
    private ArrayList<Moneta> listaMonetWAutomacie;
    private ArrayList<Moneta> listaWrzuconychMonet;

    public PrzechowywaczMonet(){
        this.listaMonetWAutomacie = new ArrayList<>();
        this.listaWrzuconychMonet = new ArrayList<>();
    }

    //Metoda dodaj do skarbonki Monete
    public void dodajDoSkarbonki(Moneta wartosc){
        this.listaMonetWAutomacie.add(wartosc);
    }

    //Metoda zwraca sumę monet, które są w skarbonce
    public double sumaWAutomacie(){
        double suma = 0;
        for (Moneta e:this.listaMonetWAutomacie)
            suma += e.getWartosc();
        return suma;
    }

    //Metoda dodaj do listy monety, które wrzuca klient
    public void wrzucMonete(Moneta wartosc){
        this.listaWrzuconychMonet.add(wartosc);
    }

    //Metoda zwraca sume monet wrzuconych przez klienta
    public double sumaWrzuconychMonet(){
        double suma = 0;
        for (Moneta m: this.listaWrzuconychMonet)
            suma += m.getWartosc();
        return suma;
    }
    
}
